export type PageSectionId = "interactive" | "differences" | "rectogesic"
export type TViewport =
  | "mobile"
  | "tablet"
  | "tablet_portrait"
  | "desktop"
  | "fullHD"

export type TViweportObject<T> = {
  [key in TViewport]: T
}
