import {
  DrawSVGPlugin,
  ScrollToPlugin,
  ScrollTrigger,
  SplitText,
} from "gsap/all"

declare module "click-outside-vue3"

declare module "#app" {
  interface NuxtApp {
    $gsap: GSAP
    $ScrollTrigger: typeof ScrollTrigger
    $ScrollToPlugin: typeof ScrollToPlugin
    $DrawSVGPlugin: typeof DrawSVGPlugin
    $SplitText: typeof SplitText
  }
}
