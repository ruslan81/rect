import { TScenes, TSceneEvents } from "utils/Rectum3D"
type ScenesData = {
  [key in TScenes]: {
    name: TScenes
    camera: {
      position: {
        x: number
        y: number
        z: number
      }
      rotation: {
        x: number
        y: number
        z: number
      }
    }
    model: {
      position: {
        x: number
        y: number
        z: number
      }
      rotation: {
        x: number
        y: number
        z: number
      }
    }
    event_name: TSceneEvents
    elements: HTMLElement[]
  }
}

type Scenes = {
  fullHD: ScenesData
  desktop: ScenesData
  tablet_portrait: ScenesData
  tablet: ScenesData
  mobile: ScenesData
}
export const scenes: Scenes = {
  get fullHD() {
    return this.desktop
  },
  desktop: {
    init: {
      name: "init",
      camera: {
        position: {
          x: 0,
          y: 0.6,
          z: 2.4,
        },
        rotation: {
          x: -0.8,
          y: 0,
          z: 0,
        },
      },
      model: {
        position: {
          x: 0,
          y: 0.7,
          z: 1.9,
        },
        rotation: {
          x: Math.PI / 4,
          y: 0,
          z: 0,
        },
      },
      event_name: "render-init-scene",
      elements: [],
    },
    first: {
      name: "first",
      camera: {
        position: {
          x: 0,
          y: 2.4,
          z: 3.9,
        },
        rotation: {
          x: -0.6,
          y: 0,
          z: 0,
        },
      },
      model: {
        rotation: {
          x: Math.PI / 4,
          y: 0,
          z: 0,
        },
        position: {
          x: 0,
          y: 0,
          z: 1,
        },
      },
      event_name: "render-first-scene",
      elements: [],
    },
    second: {
      name: "second",
      camera: {
        position: {
          x: -0.8,
          y: 0.4,
          z: 4,
        },
        rotation: {
          x: -0.3,
          y: -0.3,
          z: -0.5,
        },
      },
      model: {
        position: {
          x: -1.4,
          y: 0.2,
          z: 1.2,
        },
        rotation: {
          x: 0,
          y: 0.6,
          z: 0.3,
        },
      },
      event_name: "render-second-scene",
      elements: [],
    },
    third: {
      name: "third",
      camera: {
        position: {
          x: 1.2,
          y: -0.1,
          z: 3.8,
        },
        rotation: {
          x: -0.4,
          y: 0.7,
          z: 0.7,
        },
      },
      model: {
        position: {
          x: 1.6,
          y: 0.1,
          z: 1.3,
        },
        rotation: {
          x: -0.1,
          y: -0.6,
          z: -0.5,
        },
      },
      event_name: "render-third-scene",
      elements: [],
    },
  },
  tablet: {
    init: {
      name: "init",
      camera: {
        position: {
          x: 0,
          y: 0.8,
          z: 2.6,
        },
        rotation: {
          x: -0.8,
          y: 0,
          z: 0,
        },
      },
      model: {
        position: {
          x: 0,
          y: 0.7,
          z: 1.9,
        },
        rotation: {
          x: Math.PI / 4,
          y: 0,
          z: 0,
        },
      },
      event_name: "render-init-scene",
      elements: [],
    },
    first: {
      name: "first",
      camera: {
        position: {
          x: -0.4,
          y: 2.2,
          z: 3.9,
        },
        rotation: {
          x: -0.6,
          y: 0,
          z: 0,
        },
      },
      model: {
        rotation: {
          x: Math.PI / 4,
          y: 0,
          z: 0,
        },
        position: {
          x: 0,
          y: 0,
          z: 1,
        },
      },
      event_name: "render-first-scene",
      elements: [],
    },
    second: {
      name: "second",
      camera: {
        position: {
          x: -0.6,
          y: 0.2,
          z: 4,
        },
        rotation: {
          x: -0.3,
          y: -0.3,
          z: -0.5,
        },
      },
      model: {
        position: {
          x: -1.4,
          y: 0.2,
          z: 1.2,
        },
        rotation: {
          x: 0,
          y: 0.6,
          z: 0.3,
        },
      },
      event_name: "render-second-scene",
      elements: [],
    },
    third: {
      name: "third",
      camera: {
        position: {
          x: 1.6,
          y: 0.1,
          z: 3.8,
        },
        rotation: {
          x: -0.4,
          y: 0.7,
          z: 0.7,
        },
      },
      model: {
        position: {
          x: 1.6,
          y: 0.1,
          z: 1.3,
        },
        rotation: {
          x: -0.1,
          y: -0.6,
          z: -0.5,
        },
      },
      event_name: "render-third-scene",
      elements: [],
    },
  },
  tablet_portrait: {
    init: {
      name: "init",
      camera: {
        position: {
          x: 0,
          y: 1.4,
          z: 2.8,
        },
        rotation: {
          x: -0.8,
          y: 0,
          z: 0,
        },
      },
      model: {
        position: {
          x: 0,
          y: 0.7,
          z: 1.9,
        },
        rotation: {
          x: Math.PI / 4,
          y: 0,
          z: 0,
        },
      },
      event_name: "render-init-scene",
      elements: [],
    },
    first: {
      name: "first",
      camera: {
        position: {
          x: 0,
          y: 2.4,
          z: 3.9,
        },
        rotation: {
          x: -0.6,
          y: 0,
          z: 0,
        },
      },
      model: {
        rotation: {
          x: Math.PI / 4,
          y: 0,
          z: 0,
        },
        position: {
          x: 0,
          y: 0,
          z: 1,
        },
      },
      event_name: "render-first-scene",
      elements: [],
    },
    second: {
      name: "second",
      camera: {
        position: {
          x: -0.8,
          y: 0.4,
          z: 4,
        },
        rotation: {
          x: -0.3,
          y: -0.3,
          z: -0.5,
        },
      },
      model: {
        position: {
          x: -1.4,
          y: 0.2,
          z: 1.2,
        },
        rotation: {
          x: 0,
          y: 0.6,
          z: 0.3,
        },
      },
      event_name: "render-second-scene",
      elements: [],
    },
    third: {
      name: "third",
      camera: {
        position: {
          x: 1.8,
          y: 0.3,
          z: 3.1,
        },
        rotation: {
          x: -0.3,
          y: 0.8,
          z: 0.2,
        },
      },
      model: {
        position: {
          x: 1.6,
          y: 0.1,
          z: 1.3,
        },
        rotation: {
          x: -0.1,
          y: -0.6,
          z: -0.5,
        },
      },
      event_name: "render-third-scene",
      elements: [],
    },
  },
  mobile: {
    init: {
      name: "init",
      camera: {
        position: {
          x: 0,
          y: 2.1,
          z: 2.6,
        },
        rotation: {
          x: -0.8,
          y: 0,
          z: 0,
        },
      },
      model: {
        position: {
          x: 0,
          y: 0.7,
          z: 1.9,
        },
        rotation: {
          x: Math.PI / 4,
          y: 0,
          z: 0,
        },
      },
      event_name: "render-init-scene",
      elements: [],
    },
    first: {
      name: "first",
      camera: {
        position: {
          x: 0,
          y: 2.4,
          z: 3.9,
        },
        rotation: {
          x: -0.6,
          y: 0,
          z: 0,
        },
      },
      model: {
        rotation: {
          x: Math.PI / 4,
          y: 0,
          z: 0,
        },
        position: {
          x: 0,
          y: 0,
          z: 1,
        },
      },
      event_name: "render-first-scene",
      elements: [],
    },
    second: {
      name: "second",
      camera: {
        position: {
          x: -0.8,
          y: 0.4,
          z: 4,
        },
        rotation: {
          x: -0.3,
          y: -0.3,
          z: -0.5,
        },
      },
      model: {
        position: {
          x: -1.4,
          y: 0.2,
          z: 1.2,
        },
        rotation: {
          x: 0,
          y: 0.6,
          z: 0.3,
        },
      },
      event_name: "render-second-scene",
      elements: [],
    },
    third: {
      name: "third",
      camera: {
        position: {
          x: 1.8,
          y: 0.3,
          z: 3.1,
        },
        rotation: {
          x: -0.3,
          y: 0.8,
          z: 0.2,
        },
      },
      model: {
        position: {
          x: 1.6,
          y: 0.1,
          z: 1.3,
        },
        rotation: {
          x: -0.1,
          y: -0.6,
          z: -0.5,
        },
      },
      event_name: "render-third-scene",
      elements: [],
    },
  },
}
