export default defineNuxtPlugin(() => {
  const root = document.documentElement
  root.style.setProperty('--vh', window.innerHeight + 'px')
  root.style.setProperty('--vw', window.innerWidth + 'px')
  let timeout: string | number | NodeJS.Timeout | null = null
  window.addEventListener('resize', () => {
    timeout && clearTimeout(timeout)
    timeout = setTimeout(() => {
      root.style.setProperty('--vh', window.innerHeight + 'px')
    }, 100)
  })
})


