import GUI from 'lil-gui'
export default defineNuxtPlugin(() => {
  return {
    provide: {
      gui: GUI
    }
  }
})