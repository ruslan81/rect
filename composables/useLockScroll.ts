export function useLockScroll() {
  const isLock = ref(false)

  onMounted(() => {
    watchEffect(() => {
      if (isLock.value) {
        document.body.style.overflow = "hidden"
        document.body.style.touchAction = "none"
      } else {
        document.body.style.overflow = "auto"
        document.body.style.touchAction = "unset"
      }
    })
  })

  function lockScroll(value: boolean) {
    isLock.value = value
  }

  return {
    lockScroll,
  }
}
