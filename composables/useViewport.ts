import { TViewport } from "~~/types"
import { getViewport } from "~/utils/viewport"
export function useViewport(def: TViewport = "mobile") {
  const viewport = ref<TViewport>(def)

  onMounted(() => {
    viewport.value = getViewport()
    window.addEventListener("resize", resizeHandler)
  })
  onUnmounted(() => {
    window.removeEventListener("resize", resizeHandler)
  })

  function resizeHandler() {
    viewport.value = getViewport()
  }

  return viewport
}
