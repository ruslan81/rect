export default defineNuxtConfig({
  typescript: {
    shim: false,
  },
  devtools: { enabled: true },
  css: ["@/assets/styles/main.scss"],
  vite: {
    css: {
      preprocessorOptions: {
        scss: {
          additionalData: '@import "@/assets/styles/utils";',
        },
      },
    },
  },
  pages: true,
  imports: {
    dirs: [],
  },
  plugins: [],
  app: {
    head: {
      charset: "utf-8",
      title: "Rectogesic",
      htmlAttrs: {
        lang: "en",
      },
      meta: [
        { name: "viewport", content: "width=device-width, initial-scale=1" },
        {
          name: "twitter:card",
          content: "summary_large_image",
        },
        {
          property: "og:image:width",
          content: "600",
        },
        {
          property: "og:image:height",
          content: "314",
        },
        {
          property: "og:image:type",
          content: "image/jpeg",
        },
        {
          property: "og:site_name",
          content: "Rectogesic",
        },
        {
          property: "og:type",
          content: "website",
        },
      ],
      link: [{ rel: "icon", type: "image/jpeg", href: "/favicon.jpeg" }],
    },
  },
  runtimeConfig: {
    // тут задаємо змінні, до який не повинен бути доступ у користувача (тобто їх не видно в браузері)
    public: {
      // змінні, які можна використовувати на стороні клієнта
    },
  },
})
