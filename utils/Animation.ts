import { ScrollTrigger, gsap } from "gsap/all"
export class Animation {
  page: HTMLElement
  _scroll_triggers: ScrollTrigger[]
  constructor(page: HTMLElement) {
    this.page = page
    this._scroll_triggers = []
  }
  init() {
    this.animate()
  }
  animate() {
    const fadeinElements = document.querySelectorAll<HTMLElement>(".js-fade")
    fadeinElements.forEach((element) => {
      this._scroll_triggers.push(
        ScrollTrigger.create({
          trigger: element,
          start: () => "top 90%",
          animation: fadeIn(element),
          toggleActions: "play none none reset",
        })
      )
    })
  }
  update() {
    ScrollTrigger.refresh()
  }
  destroy() {
    console.log("destroy: ", this._scroll_triggers)
    this._scroll_triggers.forEach((st) => st.kill(true))
    this._scroll_triggers = []
  }
}

function fadeIn(el: HTMLElement) {
  return gsap.timeline().from(el, {
    opacity: 0,
    duration: 0.8,
    clearProps: "opacity",
  })
}
