import { TViewport } from "~~/types"

// type TViewportObject = {
//   [key in TViewport]: number | string | object | boolean | (() => void)
// }

export const viewportSizes = {
  mobile: 375,
  tablet_portrait: 768,
  tablet: 1024,
  desktop: 1440,
  fullHD: 1920,
} as const

export function getViewport(): TViewport
export function getViewport<T extends TViewport>(
  screen: T
): (typeof viewportSizes)[T]

export function getViewport(screen?: TViewport): number | TViewport {
  const vw = window.innerWidth

  const viewport: TViewport =
    vw < viewportSizes.tablet_portrait
      ? "mobile"
      : vw < viewportSizes.tablet
      ? "tablet_portrait"
      : vw < viewportSizes.desktop
      ? "tablet"
      : vw < viewportSizes.fullHD
      ? "desktop"
      : "fullHD"
  if (screen) {
    return viewportSizes[viewport]
  }
  return viewport
}

export function isMobile() {
  return window.innerWidth < viewportSizes.tablet_portrait
}

export function isTabletPortrait() {
  return (
    window.innerWidth < viewportSizes.tablet &&
    window.innerWidth >= viewportSizes.tablet_portrait
  )
}

export function isTablet() {
  return (
    window.innerWidth < viewportSizes.desktop &&
    window.innerWidth >= viewportSizes.tablet
  )
}

export function isDesktop() {
  return (
    window.innerWidth < viewportSizes.fullHD &&
    window.innerWidth >= viewportSizes.desktop
  )
}

export function isFullHD() {
  return window.innerWidth >= viewportSizes.fullHD
}
