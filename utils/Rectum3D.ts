import * as THREE from "three"
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls"
import { FBXLoader } from "three/examples/jsm/loaders/FBXLoader"
import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader"
import Stats from "three/examples/jsm/libs/stats.module"
import { GUI } from "lil-gui/dist/lil-gui.esm"
import { gsap } from "gsap/all"
import { scenes } from "~/data/rectum-animation-scenes"

const PI = Math.PI

export type TScenes = "init" | "first" | "second" | "third"
export type TSceneEvents =
  | "scene-start"
  | "scene-end"
  | "body-lock"
  | "body-unlock"
  | "render-init-scene"
  | "render-first-scene"
  | "render-second-scene"
  | "render-third-scene"
  | "start-init-scene"
  | "end-init-scene"
  | "start-first-scene"
  | "end-first-scene"
  | "start-second-scene"
  | "end-second-scene"
  | "start-third-scene"
  | "end-third-scene"

export default class Rectum3D {
  scenes!: {
    [key in TScenes]: {
      name: TScenes
      camera: {
        position: {
          x: number
          y: number
          z: number
        }
        rotation: {
          x: number
          y: number
          z: number
        }
      }
      model: {
        position: {
          x: number
          y: number
          z: number
        }
        rotation: {
          x: number
          y: number
          z: number
        }
      }
      event_name: TSceneEvents
      elements: HTMLElement[]
    }
  }
  group!: THREE.Group
  prev_scene_id: number
  current_scene: TScenes
  current_scene_id: number
  scenes_names: TScenes[]
  is_playing: boolean
  gui!: GUI
  container: HTMLElement
  textsContainer: HTMLElement
  camera!: THREE.PerspectiveCamera
  scene!: THREE.Scene
  renderer!: THREE.WebGLRenderer
  controls!: OrbitControls
  loader!: FBXLoader | GLTFLoader
  modelSrc: string
  model!: THREE.Group
  stats!: Stats
  pointLight!: THREE.PointLight
  ambientLight!: THREE.AmbientLight
  isInEnd: boolean

  test_mesh!: THREE.Mesh
  scene_points: {
    [key in TScenes]: {
      points: THREE.Mesh[]
    }
  }
  size!: {
    width: number
    height: number
  }
  camera_params!: {
    position: {
      x: number
      y: number
      z: number
    }
    rotation: {
      x: number
      y: number
      z: number
    }
  }
  model_params!: {
    position: {
      x: number
      y: number
      z: number
    }
    rotation: {
      x: number
      y: number
      z: number
    }
  }
  events: {
    [key in TSceneEvents]?: Set<() => void>
  }
  is_fixed: boolean
  scene_events!: {
    [key in TScenes]: {
      start: TSceneEvents
      end: TSceneEvents
      render: TSceneEvents
    }
  }
  mouse: {
    x: number
    y: number
  }

  constructor(
    container: HTMLElement,
    modelSrc: string,
    textsContainer: HTMLElement
  ) {
    this.container = container
    this.textsContainer = textsContainer
    this.isInEnd = false
    this.events = {}
    this.mouse = {
      x: 0,
      y: 0,
    }
    this.scene_points = {
      init: { points: [] },
      first: { points: [] },
      second: { points: [] },
      third: { points: [] },
    }
    this.modelSrc = modelSrc
    this.is_playing = false
    this.prev_scene_id = 0
    this.current_scene_id = 0
    this.current_scene = "init"
    this.scenes_names = ["init", "first", "second", "third"]

    this.size = {
      width: window.innerWidth,
      height: window.innerHeight,
    }
    this.textsContainer.style.width = `${this.size.width}px`
    this.textsContainer.style.height = `${this.size.height}px`
    this.is_fixed = true
    this.init().then(() => {
      this.changeScene("init")
    })
  }
  setSceneElements(scene: TScenes, elements: HTMLElement[]) {
    this.scenes[scene].elements = elements
  }
  async init() {
    this.createSceneEvents()
    this.setStartPositions()
    this.createScene()
    this.createCamera()
    this.createLight()
    this.createRenderer()
    await this.createLoader()
    // this.createStats()
    this.addPoints()

    // this.initDatGUI()

    // this.createControls()

    // this.initTestMeshGUI()

    this.animate = this.animate.bind(this)
    this.animate()

    this.setSize = this.setSize.bind(this)
    this.addEventListeners()
    // this.destroy()
  }
  createSceneEvents() {
    this.scene_events = {
      init: {
        start: "start-init-scene",
        end: "end-init-scene",
        render: "render-init-scene",
      },
      first: {
        start: "start-first-scene",
        end: "end-first-scene",
        render: "render-first-scene",
      },
      second: {
        start: "start-second-scene",
        end: "end-second-scene",
        render: "render-second-scene",
      },
      third: {
        start: "start-third-scene",
        end: "end-third-scene",
        render: "render-third-scene",
      },
    }
  }
  setStartPositions() {
    const viewport = getViewport()
    this.scenes = scenes[viewport]

    this.camera_params = this.scenes.init.camera
    this.model_params = this.scenes.init.model
  }
  addEventListeners() {
    window.addEventListener("resize", () => {
      this.setSize()
      this.camera.aspect = this.size.width / this.size.height
      this.camera.updateProjectionMatrix()
      this.renderer.setSize(this.size.width, this.size.height)
      this.textsContainer.style.width = `${this.size.width}px`
      this.textsContainer.style.height = `${this.size.height}px`
      this.emit(
        this.scenes[this.scenes_names[this.current_scene_id]].event_name
      )
      this.render()
    })
    this.container.addEventListener("pointermove", (e) => {
      this.setMouse(e.clientX, e.clientY)
    })
  }
  destroy() {
    this.events["scene-start"]?.clear()
    this.events["scene-end"]?.clear()
  }
  createLight() {
    this.pointLight = new THREE.PointLight()
    this.pointLight.position.set(1.9, 8.5, 8.8)
    this.pointLight.intensity = 0.6
    this.pointLight.castShadow = true
    this.scene.add(this.pointLight)

    this.ambientLight = new THREE.AmbientLight()
    this.ambientLight.intensity = 0.1
    this.scene.add(this.ambientLight)
  }
  createStats() {
    this.stats = new Stats()
    this.container.appendChild(this.stats.dom)
  }
  createCamera() {
    this.camera = new THREE.PerspectiveCamera(
      75,
      this.size.width / this.size.height,
      0.1,
      1000
    )
    this.camera.position.set(
      this.camera_params.position.x,
      this.camera_params.position.y,
      this.camera_params.position.z
    )
    this.camera.rotation.set(
      this.camera_params.rotation.x,
      this.camera_params.rotation.y,
      this.camera_params.rotation.z
    )
    //this.camera.lookAt(0, 0, 0)
  }
  animateScene(sceneName: TScenes) {
    if (this.is_playing) return
    this.is_playing = true
    this.emit("scene-start")
    this.emit(this.scene_events[this.current_scene_name].start)
    const scene = this.scenes[sceneName]
    gsap
      .timeline({
        onComplete: () => {
          this.is_playing = false
          if (this.current_scene_id == this.scenes_names.length - 1) {
            this.isInEnd = true
          }
          this.emit("scene-end")
          this.emit(this.scene_events[this.current_scene_name].end)
        },
      })
      .to(
        this.camera.position,
        {
          x: scene.camera.position.x,
          y: scene.camera.position.y,
          z: scene.camera.position.z,
          duration: 1,
        },
        0
      )
      .to(
        this.camera.rotation,
        {
          x: scene.camera.rotation.x,
          y: scene.camera.rotation.y,
          z: scene.camera.rotation.z,
          duration: 1,
        },
        0
      )
    if (this.current_scene_id === 0) {
      gsap.to(this.renderer.domElement, {
        opacity: 0.5,
        duration: 1,
      })
    } else {
      gsap.to(this.renderer.domElement, {
        opacity: 1,
        duration: 1,
      })
    }
  }

  createScene() {
    this.group = new THREE.Group()
    this.scene = new THREE.Scene()
    this.scene.add(this.group)
    // this.scene.background = new THREE.Color(0xeeeeee)
  }
  createRenderer() {
    this.renderer = new THREE.WebGLRenderer({
      alpha: true,
      antialias: true,
    })
    this.renderer.shadowMap.enabled = true
    this.renderer.setPixelRatio(window.devicePixelRatio ?? 1)
    this.renderer.setSize(this.size.width, this.size.height)
    this.container.appendChild(this.renderer.domElement)
    this.renderer.domElement.style.opacity = "0"
  }
  createControls() {
    this.controls = new OrbitControls(this.camera, this.renderer.domElement)
    this.controls.enableDamping = true
    this.controls.target.set(0, 0, 0)
  }
  createLoader() {
    return new Promise((resolve, reject) => {
      this.loader = new GLTFLoader()
      this.loader.load(
        // resource URL
        "./models/Rectum.glb",
        // called when the resource is loaded
        (object) => {
          this.model = object.scene
          this.model.children.forEach((child) => {
            child.receiveShadow = true
            child.castShadow = true
          })
          this.model.receiveShadow = true
          this.model.position.set(0, 0, 0)
          this.model.rotateX(this.model_params.rotation.x)
          this.scene.add(this.model)
          gsap
            .timeline()

            .to(this.renderer.domElement, {
              opacity: 0.5,
              duration: 1,
            })
          resolve(null)

          // this.initDatGUI()
        },
        // called while loading is progressing
        function (xhr) {
          console.log((xhr.loaded / xhr.total) * 100 + "% loaded")
        },
        // called when loading has errors
        function (error) {
          reject(error)
          console.log("An error happened")
        }
      )
    })
  }
  animate() {
    requestAnimationFrame(this.animate)
    if (this.controls) {
      this.controls.update()
    }
    // this.emit(this.scene_events[this.current_scene_name].render)
    this.emit("render-init-scene")
    this.emit("render-first-scene")
    this.emit("render-second-scene")
    this.emit("render-third-scene")

    this.tiltModel()
    this.getPointsCoordinates()
    this.render()
    if (this.stats) {
      this.stats.update()
    }
  }
  render() {
    this.renderer.render(this.scene, this.camera)
  }
  setSize() {
    this.size = {
      width: window.innerWidth,
      height: window.innerHeight,
    }
  }
  nextScene() {
    if (
      this.is_playing ||
      this.current_scene_id === this.scenes_names.length - 1
    ) {
      this.isInEnd = true
      return
    }
    this.current_scene_id++
    this.changeScene(this.scenes_names[this.current_scene_id])
  }
  prevScene() {
    if (this.is_playing || this.current_scene_id === 0) return
    this.current_scene_id--
    this.changeScene(this.scenes_names[this.current_scene_id])
  }
  changeScene(sceneName: TScenes) {
    this.current_scene = sceneName
    this.prev_scene_id = this.current_scene_id
    this.current_scene_id = this.scenes_names.indexOf(sceneName)
    this.animateScene(sceneName)
  }
  getSceneNames() {
    return [...this.scenes_names]
  }
  on(event: TSceneEvents, cb: () => void) {
    if (typeof this.events[event] === "undefined") {
      this.events[event] = new Set()
    }
    this.events[event]?.add(cb)
  }
  off(event: TSceneEvents, cb: () => void) {
    if (typeof this.events[event] === "undefined") return
    this.events[event]?.delete(cb)
  }
  emit(event: TSceneEvents) {
    if (typeof this.events[event] === "undefined") return
    this.events[event]?.forEach((cb) => cb())
  }
  getCurrentSceneId() {
    return this.current_scene_id
  }
  setIsInEnd(bool: boolean) {
    this.isInEnd = bool
  }
  addPoints() {
    const material = new THREE.MeshBasicMaterial({
      color: 0x00ffff,
      transparent: true,
    })
    const geo = new THREE.BoxGeometry(0, 0, 0)

    let mesh = new THREE.Mesh(geo, material)
    const point1 = new THREE.Vector3(-0.4, 0.7, 0)
    this.scene.add(mesh)

    mesh.position.set(point1.x, point1.y, point1.z)
    this.scene_points.first.points.push(mesh)

    const point2 = new THREE.Vector3(-0.36, -0.58, 1.6)
    mesh = new THREE.Mesh(geo, material)
    this.scene.add(mesh)
    mesh.position.set(point2.x, point2.y, point2.z)
    this.scene_points.first.points.push(mesh)

    const point3 = new THREE.Vector3(-0.64, -0.9, 1.93)
    mesh = new THREE.Mesh(geo, material)
    this.scene.add(mesh)
    mesh.position.set(point3.x, point3.y, point3.z)
    this.scene_points.first.points.push(mesh)

    const point4 = new THREE.Vector3(-0.35, -0.98, 1.85)
    mesh = new THREE.Mesh(geo, material)
    this.scene.add(mesh)
    mesh.position.set(point4.x, point4.y, point4.z)
    this.scene_points.first.points.push(mesh)

    this.test_mesh = mesh.clone()
    this.scene.add(this.test_mesh)

    const point5 = new THREE.Vector3(0, -0.7, 1.5)
    mesh = new THREE.Mesh(geo, material)
    this.scene.add(mesh)
    mesh.position.set(point5.x, point5.y, point5.z)
    this.scene_points.first.points.push(mesh)

    const point6 = new THREE.Vector3(0.25, -0.76, 1.65)
    mesh = new THREE.Mesh(geo, material)
    this.scene.add(mesh)
    mesh.position.set(point6.x, point6.y, point6.z)
    this.scene_points.first.points.push(mesh)
    this.scene_points.second.points.push(mesh)

    const point7 = new THREE.Vector3(0.18, -1.04, 1.66)
    mesh = new THREE.Mesh(geo, material)
    this.scene.add(mesh)
    mesh.position.set(point7.x, point7.y, point7.z)
    this.scene_points.first.points.push(mesh)

    const point8 = new THREE.Vector3(-0.09, -0.97, 1.55)
    mesh = new THREE.Mesh(geo, material)
    this.scene.add(mesh)
    mesh.position.set(point8.x, point8.y, point8.z)
    this.scene_points.third.points.push(mesh)
  }
  getPointsCoordinates() {
    // const scene = this.scenes[sceneName]
    const scenes = [this.scenes.first, this.scenes.second, this.scenes.third]
    scenes.forEach((scene) => {
      scene.elements.forEach((el, index) => {
        const vector =
          this.scene_points[scene.name].points[index].position.clone()
        vector.project(this.camera)

        vector.x = vector.x * this.size.width * 0.5
        vector.y = -(vector.y * this.size.height * 0.5)

        el.style.transform = `translate3D(${vector.x}px, ${vector.y}px, 0px)`
      })
    })

    // return this.scene_points[
    //   this.scenes_names[this.current_scene_id]
    // ].points.map((mesh) => {
    //   const vector = mesh.position.clone()
    //   vector.project(this.camera)
    //   vector.x = vector.x * window.innerWidth * 0.5
    //   vector.y = -(vector.y * window.innerHeight * 0.5)
    //   return {
    //     x: vector.x,
    //     y: vector.y,
    //   }
    // })
  }
  lockScene() {
    lockBody()
    this.is_fixed = true
  }
  unlockScene() {
    unlockBody()
    this.is_fixed = false
  }
  get current_scene_name() {
    return this.scenes_names[this.current_scene_id]
  }
  getSceneName(id: number) {
    return this.scenes_names[id]
  }
  setMouse(x: number, y: number) {
    this.mouse.x = x
    this.mouse.y = y
  }
  tiltModel() {
    if (this.is_playing || !this.camera) return
    const ndc = {
      x: ((this.mouse.x - this.size.width * 0.5) / this.size.width) * 2,
      y: -((this.mouse.y - this.size.height * 0.5) / this.size.height) * 2,
    }
    const newPosition = {
      x:
        this.scenes[this.current_scene_name].camera.rotation.x +
        PI * 0.001 * ndc.x,
      y: this.scenes[this.current_scene_name].camera.rotation.y,
      z:
        this.scenes[this.current_scene_name].camera.rotation.z +
        PI * 0.001 * ndc.y,
    }

    const diff = {
      x: this.camera.rotation.x - newPosition.x,
      y: this.camera.rotation.y - newPosition.y,
      z: this.camera.rotation.z - newPosition.z,
    }

    this.camera.rotation.set(
      this.scenes[this.current_scene_name].camera.rotation.x + diff.x * 0.9,
      this.scenes[this.current_scene_name].camera.rotation.y + diff.y * 0.9,
      this.scenes[this.current_scene_name].camera.rotation.z + diff.z * 0.9
    )
  }
  initDatGUI() {
    const { $gui } = useNuxtApp()
    this.gui = new $gui()
    this.gui
      .add(this.camera.position, "x")
      .min(-10)
      .max(10)
      .step(0.1)
      .name("camera position x")
    this.gui
      .add(this.camera.position, "y")
      .min(-10)
      .max(10)
      .step(0.1)
      .name("camera position y")
    this.gui
      .add(this.camera.position, "z")
      .min(-10)
      .max(10)
      .step(0.1)
      .name("camera position z")
    this.gui
      .add(this.camera.rotation, "x")
      .min(-2 * PI)
      .max(2 * PI)
      .step(0.1)
      .name("camera rotation x")
    this.gui
      .add(this.camera.rotation, "y")
      .min(-2 * PI)
      .max(2 * PI)
      .step(0.1)
      .name("camera rotation y")
    this.gui
      .add(this.camera.rotation, "z")
      .min(-2 * PI)
      .max(2 * PI)
      .step(0.1)
      .name("camera rotation z")

    // this.gui
    //   .add(this.scene.position, "x")
    //   .min(-10)
    //   .max(10)
    //   .step(0.1)
    //   .name("model position x")
    // this.gui
    //   .add(this.scene.position, "y")
    //   .min(-10)
    //   .max(10)
    //   .step(0.1)
    //   .name("model position y")
    // this.gui
    //   .add(this.scene.position, "z")
    //   .min(-10)
    //   .max(10)
    //   .step(0.1)
    //   .name("model position z")
    // this.gui
    //   .add(this.scene.rotation, "x")
    //   .min(-2 * PI)
    //   .max(2 * PI)
    //   .step(0.1)
    //   .name("model rotation x")
    // this.gui
    //   .add(this.scene.rotation, "y")
    //   .min(-2 * PI)
    //   .max(2 * PI)
    //   .step(0.1)
    //   .name("model rotation y")
    // this.gui
    //   .add(this.scene.rotation, "z")
    //   .min(-2 * PI)
    //   .max(2 * PI)
    //   .step(0.1)
    //   .name("model rotation z")

    // this.gui
    //   .add(this.pointLight, "intensity")
    //   .min(0)
    //   .max(1)
    //   .step(0.1)
    //   .name("light intensity")
    // this.gui
    //   .add(this.pointLight.position, "x")
    //   .min(-10)
    //   .max(10)
    //   .step(0.1)
    //   .name("light position x")
    // this.gui
    //   .add(this.pointLight.position, "y")
    //   .min(-10)
    //   .max(10)
    //   .step(0.1)
    //   .name("light position y")
    // this.gui
    //   .add(this.pointLight.position, "z")
    //   .min(-10)
    //   .max(10)
    //   .step(0.1)
    //   .name("light position z")
  }
  initTestMeshGUI() {
    const { $gui } = useNuxtApp()
    this.gui = new $gui()
    this.gui
      .add(this.test_mesh.position, "x")
      .min(-3)
      .max(3)
      .step(0.01)
      .name("x")
    this.gui
      .add(this.test_mesh.position, "y")
      .min(-3)
      .max(3)
      .step(0.01)
      .name("y")
    this.gui
      .add(this.test_mesh.position, "z")
      .min(-3)
      .max(3)
      .step(0.01)
      .name("z")
  }
}

function lockBody() {
  document.body.style.overflow = "hidden"
}

function unlockBody() {
  document.body.style.overflow = ""
}
